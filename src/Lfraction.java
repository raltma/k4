/**
 * This class represents fractions of form n/d where n and d are long integer
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

    /**
     * Main method. Different tests.
     */
    public static void main(String[] param) {
        Lfraction f1 = new Lfraction (2, -4);
        Lfraction f2 = new Lfraction(-1, 2);
        System.out.println(valueOf("2/-4"));
        System.out.println(valueOf("2/4/"));


    }

    private long numerator;
    private long denominator;

    /**
     * Constructor.
     *
     * @param a numerator
     * @param b denominator > 0
     */
    public Lfraction(long a, long b) {
        numerator = a;
        denominator = b;
        if (denominator == 0){
            throw new RuntimeException("Denominator can not be 0");
        }
        reduce();
    }

    /**
     * Public method to access the numerator field.
     *
     * @return numerator
     */
    public long getNumerator() {
        return numerator;
    }

    /**
     * Public method to access the denominator field.
     *
     * @return denominator
     */
    public long getDenominator() {
        return denominator;
    }

    /**
     * Conversion to string.
     *
     * @return string representation of the fraction
     */
    @Override
    public String toString() {
        return numerator + "/" + denominator;
    }

    /**
     * Equality test.
     *
     * @param m second fraction
     * @return true if fractions this and m are equal
     */
    @Override
    public boolean equals(Object m) {
        if(this == m){
            return true;
        }
        if (m instanceof Lfraction){
            Lfraction frc = (Lfraction)m;
            return compareTo(frc) == 0;
        }
        return false;
    }

    /**
     * Hashcode has to be equal for equal fractions.
     *
     * @return hashcode
     */
    @Override
    public int hashCode() {
        return 37 * (37 * 17 + (int)numerator) + (int)denominator;
    }

    public long calculateGreatestDivisor(long a, long b) {
        if (a % b == 0) {
            return b;
        }
        return calculateGreatestDivisor(b, a % b);
    }

    void reduce() {
        long gcd = calculateGreatestDivisor(numerator, denominator);
        numerator /= gcd;
        denominator /= gcd;
        if(denominator < 0){
            numerator /= -1;
            denominator /= -1;
        }
    }

    /**
     * Sum of fractions.
     *
     * @param m second addend
     * @return this+m
     */
    public Lfraction plus(Lfraction m) {
        long newNumerator = numerator * m.getDenominator() + m.getNumerator() * denominator;
        long newDenominator = denominator * m.getDenominator();
        return new Lfraction(newNumerator, newDenominator);
    }

    /**
     * Multiplication of fractions.
     *
     * @param m second factor
     * @return this*m
     */
    public Lfraction times(Lfraction m) {
        long newNumerator = numerator * m.getNumerator();
        long newDenominator = denominator * m.getDenominator();
        return new Lfraction(newNumerator, newDenominator);
    }

    /**
     * Inverse of the fraction. n/d becomes d/n.
     *
     * @return inverse of this fraction: 1/this
     */
    public Lfraction inverse() {
        if(numerator == 0){
            throw new RuntimeException("Inversing 0 to denominator is not allowed");
        }
        long newNumerator = denominator;
        long newDenominator = numerator;
        return new Lfraction(newNumerator, newDenominator);
    }

    /**
     * Opposite of the fraction. n/d becomes -n/d.
     *
     * @return opposite of this fraction: -this
     */
    public Lfraction opposite() {
        long newNumerator = -numerator;
        long newDenominator = denominator;
        return new Lfraction(newNumerator, newDenominator);
    }

    /**
     * Difference of fractions.
     *
     * @param m subtrahend
     * @return this-m
     */
    public Lfraction minus(Lfraction m) {
        return plus(m.opposite());
    }

    /**
     * Quotient of fractions.
     *
     * @param m divisor
     * @return this/m
     */
    public Lfraction divideBy(Lfraction m) {
        if(m.getNumerator() == 0){
            throw new RuntimeException("Division by 0 is not allowed");
        }
        return times(m.inverse());
    }

    /**
     * Comparision of fractions.
     *
     * @param m second fraction
     * @return -1 if this < m; 0 if this==m; 1 if this > m
     */
    @Override
    public int compareTo(Lfraction m) {
        long nOd = numerator * m.getDenominator();
        long dOn = denominator * m.getNumerator();
        return Long.compare(nOd, dOn);
    }

    /**
     * Clone of the fraction.
     *
     * @return new fraction equal to this
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return new Lfraction(numerator, denominator);
    }

    /**
     * Integer part of the (improper) fraction.
     *
     * @return integer part of this fraction
     */
    public long integerPart() {
        return numerator/denominator;
    }

    /**
     * Extract fraction part of the (improper) fraction
     * (a proper fraction without the integer part).
     *
     * @return fraction part of this fraction
     */
    public Lfraction fractionPart() {
        return new Lfraction(numerator%denominator, denominator);
    }

    /**
     * Approximate value of the fraction.
     *
     * @return numeric value of this fraction
     */
    public double toDouble() {
        return (double) numerator / denominator;
    }

    /**
     * Double value f presented as a fraction with denominator d > 0.
     *
     * @param f real number
     * @param d positive denominator for the result
     * @return f as an approximate fraction of form n/d
     */
    public static Lfraction toLfraction(double f, long d) {
        long newNumerator = Math.round(f*d);
        return new Lfraction(newNumerator, d);
    }

    /**
     * Conversion from string to the fraction. Accepts strings of form
     * that is defined by the toString method.
     *
     * @param s string form (as produced by toString) of the fraction
     * @return fraction represented by s
     */
    public static Lfraction valueOf(String s) {
        long newNumerator = 0;
        long newDenominator = 0;
        if (s.substring(s.length()-1).equals("/")) {
            throw new RuntimeException("String must not end with \"/\"");
        }
        String[] inputArray = s.split("/");
        if(inputArray.length > 2){
            throw new RuntimeException("String \"" + s + "\" is invalid");
        }
        try {
            newNumerator = Integer.parseInt(s.split("/")[0]);
            newDenominator = Integer.parseInt(s.split("/")[1]);
        } catch (NumberFormatException e) {
            throw new RuntimeException("String \"" + s + "\" is invalid, it has invalid symbols");
        }
        return new Lfraction(newNumerator, newDenominator);
    }
}

